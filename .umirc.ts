/*
 * @Author: 汪洋
 * @Date: 2021-10-09 11:54:32
 * @LastEditTime: 2021-10-09 15:42:28
 * @LastEditors: 汪洋
 * @Description:
 * @FilePath: \document\.umirc.ts
 */
import { defineConfig } from 'dumi';

export default defineConfig({
  title: 'JavaScript',
  favicon:
    'https://user-images.githubusercontent.com/9554297/83762004-a0761b00-a6a9-11ea-83b4-9c8ff721d4b8.png',
  logo: 'https://user-images.githubusercontent.com/9554297/83762004-a0761b00-a6a9-11ea-83b4-9c8ff721d4b8.png',
  outputPath: 'docs-dist',
  locales: [['zh-CN', '中文']],
  // more config: https://d.umijs.org/config
});
