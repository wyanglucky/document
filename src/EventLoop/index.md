---
title: EventLoop
order: 2
toc: menu
---

## <span style="color:#135ce0">JavaScript 代码执行机制</span>

1. 遇到`同步代码`直接执行
2. 遇到`异步代码`先放一边，并且将他`回调函数`存起来，存的地方叫`事件队列`
3. 等所有`同步代码`都执行完，再从`事件队列`中把存起来的所有`异步回调函数`拿出来按顺序执行

![执行机制](./img/1.webp)

```tsx | pure
console.log(1) // 同步
setTimeout(() => {
  console.log(2) // 异步
}, 2000);
console.log(3) // 同步
setTimeout(() => {
  console.log(4) // 异步
}, 0);
console.log(5) // 同步

输出 ： 1 3 5 4 2
```
![执行机制](./img/2.webp)

## <span style="color:#135ce0">宏任务&&微任务</span>

1. 所有`同步代码`都执行完毕后，再从`事件队列里`依次执行所有`异步回调函数`

2. `事件队列`是用来存异步回调的，异步也分类型，异步任务分为`宏任务`和`微任务`，并且**微任务执行时机先于宏任务**

下面是宏任务和微任务的分别

### <span style="color:#135ce0">宏任务</span>

| #                                                        | 浏览器 | Node |
| :------------------------------------------------------- | :----- | :--- |
| <span style="color:#135ce0">I/O</span>                   | ✔      | ✔    |
| <span style="color:#135ce0">setTimeout</span>            | ✔      | ✔    |
| <span style="color:#135ce0">setInterval</span>           | ✔      | ✔    |
| <span style="color:#135ce0">setImmediate</span>          | ❌     | ✔    |
| <span style="color:#135ce0">requestAnimationFrame</span> | ✔      | ❌   |

### <span style="color:#135ce0">微任务</span>

| #                                                                       | 浏览器 | Node |
| :---------------------------------------------------------------------- | :----- | :--- |
| <span style="color:#135ce0">Promise.prototype.then catch finally</span> | ✔      | ✔    |
| <span style="color:#135ce0">process.nextTick</span>                     | ❌     | ✔    |
| <span style="color:#135ce0">MutationObserver</span>                     | ✔      | ❌   |

### <span style="color:#135ce0">执行流程</span>

![执行机制](./img/3.webp)

1. 标记出`异步`和`同步`
2. 异步中，标记出`宏任务`和`微任务`
3. 分轮数，一轮一轮慢慢走

```tsx | pure
console.log(1); // 同步
setTimeout(() => {
  console.log(2); // 异步：宏任务
});
console.log(3); // 同步
Promise.resolve().then(() => {
  // 异步：微任务
  console.log(4);
});
console.log(5); // 同步
```

第一轮

- 说明：先把同步的执行输出
- 输出：1，3，5
- 产生宏任务：`setTimeout`，产生微任务：`Promise.prototype.then`

第二轮

- 说明：微任务先执行
- 输出：4
- 产生宏任务：无，产生微任务：无
- 剩余宏任务：`setTimeout`，剩余微任务：无

第三轮（结束）

- 说明：执行宏任务
- 输出：2
- 产生宏任务：无，产生微任务：无
- 剩余宏任务：无，剩余微任务：无
