/*
 * @Author: 汪洋
 * @Date: 2021-10-09 11:54:32
 * @LastEditTime: 2021-10-09 14:22:01
 * @LastEditors: 汪洋
 * @Description:
 * @FilePath: \document\src\index.ts
 */
export { default as EventLoop } from './EventLoop';
